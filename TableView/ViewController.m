//
//  ViewController.m
//  TableView
//
//  Created by Click Labs133 on 9/28/15.
//  Copyright (c) 2015 dk. All rights reserved.
//

#import "ViewController.h"
//.m
@interface ViewController ()
{
    NSMutableArray *cellArray;
}
@property (strong, nonatomic) IBOutlet UITableView *currentTableView;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (strong, nonatomic) IBOutlet UIButton *submit;

@end

@implementation ViewController
@synthesize currentTableView , textField,addButton;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    cellArray=[[NSMutableArray alloc]init];
    textField.hidden=true;
    self.submit.hidden=true;

    //  [self.submit setTitle:@"HELLO" forState:UIControlStateNormal];
    // [self.submit actionsForTarget:@selector(buttonClicked:) forControlEvent:UIControlEventTouchUpInside];

}
- (IBAction)addButtonAction:(id)sender {
    textField.hidden=false;
    self.submit.hidden=false;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cellArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    //[self performSegueWithIdentifier:@"goToScreen2" sender:self];
    cell.textLabel.text=cellArray[indexPath.row];
    
    cell.textColor=[UIColor redColor];
    cell.textLabel.textAlignment= UITextAlignmentCenter;
        return cell;
    
    
}
-(IBAction)buttonClicked:(UIButton *)sender{
    
    
    [cellArray addObject:textField.text];
    [currentTableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
