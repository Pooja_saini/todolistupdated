//
//  AppDelegate.h
//  TableView
//
//  Created by Click Labs133 on 9/28/15.
//  Copyright (c) 2015 dk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

